import React from 'react';
import './App.css';
import { Footer } from './Components/Footer';
import { Route , BrowserRouter , Switch } from 'react-router-dom';
import {  Prestations } from "./Components/Prestations";
import { Main } from './Components/Main';



function App() {
  return (
    
<BrowserRouter>
  
      <Switch>
        <Route path = '/' component ={  Main  } exact />
        <Route path = '/prestations' component = { Prestations }/>
        
      </Switch>

      <Footer />
</BrowserRouter>
 
 
  );
}

export default App;
