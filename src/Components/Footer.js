import React , { Component } from 'react';
import  { Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import Modal from 'react-awesome-modal';

export class Footer extends Component {

  constructor(props){
    super(props);
    this.state = {
               visible: false
    }
    this.montre = this.montre.bind(this)
    this.fermeture = this.fermeture.bind(this)
}

montre(){
       this.setState( {visible: true} )
}

fermeture(){
       this.setState({ visible : false })
}


    render(){

        return(
        <div>
        <nav className="navbar navbar-expand-lg  navbar-dark fixed-top" id="mainNav">
            <div className="container">
            <NavLink className="navbar-brand js-scroll-trigger" to="#page-top">Nanda Technologie</NavLink>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i className="fas fa-bars" />
            </button>
            <div className="collapse navbar-collapse " id="navbarResponsive">
                <ul className="navbar-nav text-uppercase ml-auto">
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#services" style = {{}}><strong>Accueil</strong></a>
                </li>
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#portfolio"><strong>Boutique</strong></a>
                </li>
                
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#team"><strong>Equipes</strong></a>
                </li>

                      
                <li className="nav-item">
                    <NavLink className="nav-link js-scroll-trigger" to="/prestations/"><strong>Prestations</strong></NavLink>
                </li>
                     { /* 
                <li className="nav-item">
                    <NavLink className="nav-link js-scroll-trigger" to ="/depannages/">Depannages</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link js-scroll-trigger" to="/formations/">Formations</NavLink>
                </li>
                      */}

                
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#contact"><strong>Contact</strong></a>
                </li>
                </ul>
              
            </div>
            </div>
        </nav>
        {/* Footer */}
        <footer className="footer">
            <div className="container">
            <div className="row align-items-center">
            <div className="col-lg-7">
          <p className>© 2019 Nanda Technologie. tout droits reservés | developpé avec le  <i className ="fa fa-heart"  style={{color: "red"}}></i> par R4y
            <a href=""></a>
          </p>
        </div>
               
                <div className="col-md-4">
                <ul className="list-inline quicklinks">
                    <li className="list-inline-item">
                    <a href="#">Tout droits</a>
                    </li>
                    <li className="list-inline-item">
                    <a href="#">reservés</a>
                    </li>
                </ul>
                </div>
            </div>
            </div>
        </footer>
        </div>

        )
    }
}