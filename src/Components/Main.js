import React,{ Component } from "react";
import { Button } from 'react-bootstrap';
import Modal from 'react-awesome-modal';
import { NavLink , Link } from 'react-router-dom';
import { Modals } from './Modals'


export class Main extends Component {

  constructor(props){
    super(props);
    this.state = {
               visible: false,

               name : "",
               email : "",
               phone : "",
               message : "",
               mailSent : false,
               erreur : null           

    }
    this.montre = this.montre.bind(this)
    this.fermeture = this.fermeture.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
}

handleFormSubmit( event ) {
  event.preventDefault();
  console.log(this.state);
}

montre(){
       this.setState( {visible: true} )
}

fermeture(){
       this.setState({ visible : false })
}


      render(){
         return(
        <div> 
                 <div>
  {/* Navigation */}
        <nav className="navbar navbar-expand-lg  navbar-dark fixed-top" id="mainNav">
            <div className="container">
            <NavLink className="navbar-brand js-scroll-trigger" to="#page-top">Nanda Technologie</NavLink>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i className="fas fa-bars" />
            </button>
            <div className="collapse navbar-collapse " id="navbarResponsive">
                <ul className="navbar-nav text-uppercase ml-auto">
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#services" style = {{}}><strong>Accueil</strong></a>
                </li>
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#portfolio"><strong>Boutique</strong></a>
                </li>
                
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#team"><strong>Equipes</strong></a>
                </li>

                      
                <li className="nav-item">
                    <NavLink className="nav-link js-scroll-trigger" to="/prestations/"><strong>Prestations</strong></NavLink>
                </li>
                     { /* 
                <li className="nav-item">
                    <NavLink className="nav-link js-scroll-trigger" to ="/depannages/">Depannages</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link js-scroll-trigger" to="/formations/">Formations</NavLink>
                </li>
                      */}

                
                <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/#contact"><strong>Contact</strong></a>
                </li>
                </ul>
              
            </div>
            </div>
        </nav>
        {/* Header */}
        <header className="masthead">
            <div className="container">
            <div className="intro-text main-header">
                
                <div className="intro-heading text-uppercase">Nanda Technologie.</div>
                <a className="button-style btn btn-warning btn-xl text-uppercase js-scroll-trigger" href ="/#services">Plus d'informations</a>
                          &nbsp;
                          &nbsp;
                <a className="button-style btn btn-info btn-xl text-uppercase js-scroll-trigger" href ="/#contact">Demander un devis</a>
            </div>
            </div>          
        </header>
          
    </div>  
            <div classname="services">
            {/* Acceuil */}
            <section className="page-section" id="services">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <h2 className="section-heading text-uppercase" style = {{ color : 'black'}}><strong>NOS Services</strong></h2>
                    <h3 className="section-subheading text-muted"></h3>
                  </div>
                </div>
                <div className="row text-center">
                  <div className="col-md-4">         
                     <img className="image rounded-circle" src="img/team/4.jpg" alt />         
                    <h4 className="service-heading">Conception d'applications</h4>
                    <p className="text-muted">Nous créons votre application, site Vitrine ou E-commerce, sur mesure. 
                      Avec un résultat de qualité professionnelle et un budget maitrisé .</p>
                    <p><strong>Offrez-vous une vitrine ouverte sur le monde !</strong></p>
                   <Link className="button-style btn btn-warning btn-xl text-uppercase" to="/prestations">En savoir plus</Link>
                  </div>
          
                  <div className="col-md-4">
                    <img className="image rounded-circle" src="img/team/5.jpg" alt /> 
                    <h4 className="service-heading">Depannages informatiques</h4>
                    <p className="text-muted">Optimisation de votre ordinateur.
                      Suppression des Virus et de logiciels indésirables.
                      Sauvegarde des données.
                      Changement/Ajout de pièces.</p>
                      <p><strong>Travaillez dans des conditions optimales!</strong></p>
                    <Link className="button-style btn btn-warning btn-xl text-uppercase" to="/prestations">En savoir plus</Link>
                 
                  </div>
                  <div className="col-md-4">
                   <img className="image rounded-circle" src="img/team/6.jpg" alt /> 
                    <h4 className="service-heading">Formations informatiques</h4>
                    <p className="text-muted">     
                        Traitement de texte, Création d’un Site Web, Hebergement de votre applications web,
                        Administrations de serveurs Windows et Linux.</p>
                      <p><strong>Offrez-vous les moyens de reussir</strong></p>
                    <Link className="button-style btn btn-warning btn-xl text-uppercase" to="/prestations">En savoir plus</Link>
                  </div>
                  
                </div>
              </div>
            </section>
            </div>


            <div classname="boutique">
            {/* Portfolio Grid */}
            <section className="bg-light page-section" id="portfolio">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <h2 className="section-heading text-uppercase" style = {{ color : 'black'}}> <strong>Boutique</strong></h2>
                    <h3 className="section-subheading text-muted"><strong>Stocks des marchandises disponibles dans nos magasins.Commandez,et faites vous livrer.</strong></h3>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-4 col-sm-6 portfolio-item">
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                      <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                          <i className="fas fa-plus fa-3x" />
                        </div>
                      </div>
                      <img className="img-fluid" src="img/portfolio/01-thumbnail.jpg" alt />
                    </a>
                    <div className="portfolio-caption">
                      <h4>Imprimante hp</h4>
                      <p  style = {{ color :"red"}}><strike>35000 Fcfa</strike> &nbsp;&nbsp;25.000 Fcfa</p>
                    </div>
          
                  </div>
                  <div className="col-md-4 col-sm-6 portfolio-item">
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                      <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                          <i className="fas fa-plus fa-3x" />
                        </div>
                      </div>
                      <img className="img-fluid" src="img/portfolio/02-thumbnail.jpg" alt />
                    </a>
                    <div className="portfolio-caption">
                      <h4>Ordinateur portable</h4>
                      <p  style = {{ color :"red"}}><strike>200.000 Fcfa</strike> &nbsp;&nbsp; 150.000 Fcfa</p>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-6 portfolio-item">
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                      <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                          <i className="fas fa-plus fa-3x" />
                        </div>
                      </div>
                      <img className="img-fluid" src="img/portfolio/03-thumbnail.jpg" alt />
                    </a>
                    <div className="portfolio-caption">
                      <h4>Point d'acces D-LINK</h4>
                      <p  style = {{ color :"red"}}><strike>36000 Fcfa</strike> &nbsp;&nbsp;24.000 Fcfa</p>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-6 portfolio-item">
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                      <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                          <i className="fas fa-plus fa-3x" />
                        </div>
                      </div>
                      <img className="img-fluid" src="img/portfolio/04-thumbnail.jpg" alt />
                    </a>
                    <div className="portfolio-caption">
                      <h4>Ordinateur Bureau HP 290 G2 core I3</h4>
                      <p  style = {{ color :"red"}}><strike>300.000 Fcfa</strike> &nbsp;&nbsp;240.000 Fcfa</p>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-6 portfolio-item">
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                      <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                          <i className="fas fa-plus fa-3x" />
                        </div>
                      </div>
                      <img className="img-fluid" src="img/portfolio/05-thumbnail.jpg" alt />
                    </a>
                    <div className="portfolio-caption">
                      <h4>Climatiseur NASCO 2chvx</h4>
                      <p  style = {{ color :"red"}}><strike>315.000 Fcfa</strike> &nbsp;&nbsp; 210.000 Fcfa</p>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-6 portfolio-item">
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                      <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                          <i className="fas fa-plus fa-3x" />
                        </div>
                      </div>
                      <img className="img-fluid" src="img/portfolio/06-thumbnail.jpg" alt />
                    </a>
                    <div className="portfolio-caption">
                      <h4>Antivirus Kaspersky Internet Security 3 POSTES</h4>
                      <p style = {{ color :"red"}}><strike>32000 Fcfa</strike> &nbsp;&nbsp;20.000 Fcfa</p>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>


{/* Team */}


<div classname="contact">
  {/* Contact */}
  <section className="page-section" id="contact">
    <div className="container">
      <div className="row">
        <div className="col-lg-12 text-center">
          <h2 className="section-heading text-uppercase" style = {{ color : 'gold'}} ><strong>Nous contacter</strong></h2>
          <h3 className="section-subheading" style = {{ color : 'white'}}>Pour toutes vos preoccupations,vous pouvez nous ecrire par email</h3>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <form id="contactForm" name="sentMessage" noValidate="novalidate" action = "/contact.php">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <input className="form-control"
                      value = { this.props.name } id="name" 
                      onChange = {e => this.setState({ name : e.target.value }) }
                      type="text" placeholder="Votre nom *"
                       required="required" 
                       data-validation-required-message="Entrez votre nom svp." 
                    />
                  <p className="help-block text-danger" />
                </div>
                <div className="form-group">
                  <input className="form-control" 
                    id="email" type="email" placeholder="Votre email *"
                    value = { this.props.email }
                    onChange = {e=> this.setState({ email : e.target.value }) }
                    required="required" 
                    data-validation-required-message="Enrez un email svp." 
                  />
                  <p className="help-block text-danger" />
                </div>
                <div className="form-group">
                  <input className="form-control" 
                    id="phone" type="tel" placeholder="Votre numero *" 
                    required="required" 
                    value = { this.props.phone }
                    onChange = {e=> this.setState({ phone : e.target.value }) }
                    data-validation-required-message="Entrez un numero de telephone svp."
                  />
                  <p className="help-block text-danger" />
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <textarea className="form-control"
                   id="message" placeholder="Votre message *" 
                   value = { this.props.message }
                   onChange = {e=> this.setState({ message : e.target.value }) }
                   required="required"
                   data-validation-required-message="Entrez un message svp." defaultValue={""} 
                  />
                  <p className="help-block text-danger" />
                </div>
              </div>
              <div className="clearfix" />
              <div className="col-lg-12 text-center">
                <div id="success" />
                 <button id="sendMessageButton" onClick = {e => this.handleFormSubmit (e)} className="button-style  btn btn-warning btn-xl text-uppercase " type="submit">Envoyez votre message</button>
               </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    < Modals />
  </section>
</div>

</div>
         
        )
    }

}



           