import React , { Component } from 'react';
import { NavLink } from 'react-router-dom';

export class  Prestations extends Component {
    
    render(){
        return (
         <div>
          <div>
          {/* Navigation */}
                <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                    <div className="container">
                    <NavLink className="navbar-brand js-scroll-trigger" to="#page-top">Maintenance-ci</NavLink>
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        Menu
                        <i className="fas fa-bars" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarResponsive">
                        <ul className="navbar-nav text-uppercase ml-auto">
                        <li className="nav-item">
                            <a className="nav-link js-scroll-trigger" href="/#services" style = {{}}><strong>Accueil</strong></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link js-scroll-trigger" href="/#portfolio"><strong>Boutique</strong></a>
                        </li>
                        
                        <li className="nav-item">
                            <a className="nav-link js-scroll-trigger" href="/#team"><strong>Equipes</strong></a>
                        </li>
        
                              
                        <li className="nav-item">
                            <NavLink className="nav-link js-scroll-trigger" to="/prestations/"><strong>Prestations</strong></NavLink>
                        </li>
                             { /* 
                        <li className="nav-item">
                            <NavLink className="nav-link js-scroll-trigger" to ="/depannages/">Depannages</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link js-scroll-trigger" to="/formations/">Formations</NavLink>
                        </li>
                              */}
        
                        
                        <li className="nav-item">
                            <a className="nav-link js-scroll-trigger" href="/#contact"><strong>Contact</strong></a>
                        </li>
                        </ul>                      
                        
                    </div>
                    </div>
                </nav>
                {/* Fin navigation */}

          <header className="masthead ">
            <div className="container">
            <div className="intro-text  prestation-header">
                
                <div className="intro-heading text-uppercase">PRESTATIONS </div>
              
            </div>
            </div>          
        </header>
                
            </div> 
  <div className="container">
    <div className=" text-center mt-10  card-in">
       <h3><strong> NOS PRESTATIONS </strong></h3>
       <p> Nous sommes spécialisés dans le développement de sites et d'applications web, dans l'infogérance et dans l'hébergement et la location de serveurs. Nous vous proposons également la vente et la maintenance de matériels informatique. 
       <br />  N'hésitez pas à nous contacter pour plus d'informations.
       </p>       
    </div> 
    <hr />      
     
  <div className="featured-boxes ">
  <div className="row text-center">
    <div className="col-md-6 col-sm-12">
     <div className="card bg-light card-border  card-in"> 
      <div className="mt-lg-5 mt-0" style={{height: 419}}>
        <div className="box-content text-center">
          <span class="fa-stack fa-4x">
            <i className ="fas fa-circle fa-stack-2x " style = {{ color:'black'}}/>
            <i className="fas fa-server fa-stack-1x fa-inverse " />
          </span>
          <h4 className="text-uppercase">Hebergement</h4>
          <p className="text-center">
            Notre équipe d’experts est formée pour vous offrir des solutions et gérer votre réseau informatique de façon optimisée. Gestion de serveurs, des systèmes de sauvegardes ou encore mise en place d’infrastructures réseau, vos besoins seront étudiés et feront l’objet  de devis rapides et entièrement personnalisables. Contactez-nous !
          </p>
      </div> 
        </div>
      </div>
    </div>

    <div className="col-md-6 col-sm-12">
    <div className="card bg-light card-border  card-in"> 
      <div className="mt-lg-5 mt-0" style={{height: 419}}>
        <div className="box-content text-center">
          <span class="fa-stack fa-4x">
           <i className ="fas fa-circle fa-stack-2x " style = {{ color:'orange'}}/>
           <i className="fas fa-download fa-stack-1x fa-inverse" />
          </span>
          <h4 className="text-uppercase" style = {{ color:'orange'}}>Depannages d'ordinateurs</h4>
          <p className="text-center">
            À l'ère de l’information, vos données n’ont jamais eu autant de valeur, et toute votre activité dépend de la sécurité de votre infrastructure. Nos experts vous accompagnent afin de sécuriser vos éléments les plus importants par le biais de solutions complètes : duplication, chiffrement ou encore monitoring de votre activité, ne craignez plus de perdre vos données.
          </p>
        </div>
      </div>
      </div>
    </div>


    <div className="col-md-6 col-sm-12">
    <div className="card bg-light card-border  card-in">
      <div className="mt-lg-5" style={{height: 442}}>
        <div className="box-content text-center">
        <span class="fa-stack fa-4x">
          <i className ="fas fa-circle fa-stack-2x" style = {{ color:'black'}} />
          <i className="fas fa-list-alt fa-stack-1x fa-inverse " />
        </span>
          <h4 className="text-uppercase">Création de sites web</h4>
          <p className="text-center">
            La mise en place d’un site internet pour mettre en avant votre projet ou votre entreprise est devenu un atout de marketing direct incontournable pour développer votre image et votre visibilité. Grâce à nos conseils et à notre expertise, maîtrisez votre vitrine 2.0. Grâce à notre savoir-faire, Diamontis informatique vous offre aujourd’hui la possibilité de vous accompagner dans votre entreprise de l’idée jusqu’à sa concrétisation en vous offrant là encore des solutions web complètes : design, hébergement (partenaire OVH certifié), référencement, maintenance…
          </p>
        </div>
      </div>
      </div>
    </div>

    <div className="col-md-6 col-sm-12">
    <div className="card bg-light card-border  card-in">
    <div className="mt-lg-5" style={{height: 442}}>
        <div className="box-content text-center">
        <span class="fa-stack fa-4x">
          <i className ="fas fa-circle fa-stack-2x" style = {{ color:'orange'}}/>
          <i className="fas fa-desktop fa-stack-1x fa-inverse " />
        </span>
          <h4 className="text-uppercase" style = {{ color:'orange'}}>Développement d’applications web et mobile</h4>
          <p className="text-center">
            Tout comme la création de sites web, nos experts mettent un point d’honneur à proposer une solution pleinement adaptée aux besoins du client en matière de développement d’applications en ligne ou localement sur votre ordinateur. Nous partons d’un cahier des charges bien établi afin que le produit final vous offre une satisfaction maximum.
          </p>
        </div>
      </div>
      </div>
    </div>

    <div className="col-md-6 col-sm-12">
    <div className="card bg-light card-border  card-in">
      <div className="mt-lg-5" style={{height: 392}}>
        <div className="box-content text-center">
        <span class="fa-stack fa-4x">
          <i className ="fas fa-circle fa-stack-2x " style = {{ color:'black'}} />
          <i className="fas fa-sitemap fa-stack-1x fa-inverse" />
        </span>
          <h4 className="text-uppercase">Installation de réseaux informatiques</h4>
          <p className="text-center">
            L’informatique étant notre coeur de métier, maintenance-ci propose tout naturellement des solutions de mise en place ou d’optimisation de réseaux informatiques pour les TPE/PME.
             Un expert pourra se déplacer directement sur site afin de faire un premier état des lieux et vous conseiller sur les démarches à suivre pour un fonctionnement optimal de votre réseau.
          </p>
        </div>
        </div>
      </div>
    </div>
     <div className="col-md-6 col-sm-12">
     <div className="card bg-light card-border card-in">
      <div className="mt-lg-5" style={{height: 392}}>
        <div className="box-content text-center">
        <span class="fa-stack fa-4x">
          <i className ="fas fa-circle fa-stack-2x "  style = {{ color:'orange'}} />
          <i className="fas fa-building fa-stack-1x fa-inverse" />
        </span>
          <h4 className="text-uppercase" style = {{ color:'orange'}}> Installation de salle serveur</h4>
          <p className="text-center">
            Votre entreprise se développe, vous nécessitez une puissance de calcul accrue.
            Vous souhaitez sauvegarder tous vos postes en interne.
            Maintenace-ci conçoit et installe votre salle serveur ou modifie votre salle existante.
          </p>
        </div>
      </div>
      </div>
    </div>
    
     <div className="col-sm-12">
     <div className="card bg-light card-border  card-in">
      <div className="mt-lg-5" style={{height: 342}}>
        <div className="box-content text-center">
        <span class="fa-stack fa-4x">
          <i className ="fas fa-circle fa-stack-2x "  style = {{ color:'black'}} />
          <i className=" fas fa-book fa-stack-1x fa-inverse" />
        </span>
          <h4 className="text-uppercase text-center">Formation </h4>
          <p className="text-center">
            Votre entreprise se développe, vous nécessitez une puissance de calcul accrue.
            Vous souhaitez sauvegarder tous vos postes en interne.
            Maintenace-ci conçoit et installe votre salle serveur ou modifie votre salle existante.
          </p>
        </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
</div>
        )
    }
}